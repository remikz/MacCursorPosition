// Public Domain License 2016
//
// Terminate with Ctrl+C

#include <ApplicationServices/ApplicationServices.h>

static CGEventRef mouseMoveCallback (
    CGEventTapProxy proxy,
    CGEventType type,
    CGEventRef event,
    void * refcon
) {
	CGPoint mouseLocation = CGEventGetLocation( event );
	printf( "%u %u\n", (unsigned int) mouseLocation.x, (unsigned int) mouseLocation.y );

    // Pass on the event, we must not modify it anyway, we are a listener
    return event;
}

int main (
    int argc,
    char ** argv
) {
    CGEventMask emask;
    CFMachPortRef myEventTap;
    CFRunLoopSourceRef eventTapRLSrc;

	printf("Quit with Ctrl+C\n");

    // We only want "other" mouse button click-release, such as middle or exotic.
    // Ignores left and right mouse buttons.
    emask = CGEventMaskBit( kCGEventMouseMoved );

    // Create the Tap
    myEventTap = CGEventTapCreate (
        kCGSessionEventTap,          // Catch all events for current user session
        kCGTailAppendEventTap,       // Append to end of EventTap list
        kCGEventTapOptionListenOnly, // We only listen, we don't modify
        emask,
        & mouseMoveCallback,
        NULL                         // We need no extra data in the callback
    );

    // Create a RunLoop Source for it
    eventTapRLSrc = CFMachPortCreateRunLoopSource(
        kCFAllocatorDefault,
        myEventTap,
        0
    );

    // Add the source to the current RunLoop
    CFRunLoopAddSource(
        CFRunLoopGetCurrent(),
        eventTapRLSrc,
        kCFRunLoopDefaultMode
    );
    
    // Keep the RunLoop running forever
    CFRunLoopRun();

    // Not reached (RunLoop above never stops running)
    return 0;
}
