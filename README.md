MacCursorPosition - Print Mouse Cursor Position for Mac OSX

## Overview
This simply prints to the terminal your current mouse x,y coordinates, all for free.

## Usage
Run this program in any terminal on your mac.

## Building

	make build
	make run

## License
Public Domain 2016

