.PHONEY: build clean run
APP = maccursorposition

all: help

help: 
	@echo make [ clean build run ]

build: $(APP)

clean:
	-rm -f $(APP) 

$(APP): $(APP).c
	gcc -O2 -framework ApplicationServices -o $@ $< 

run:
	./$(APP)
